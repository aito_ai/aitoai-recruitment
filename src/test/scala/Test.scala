import org.scalatest.{FlatSpec, Matchers}

class MainTest extends FlatSpec with Matchers {

  "MainTest" should "execute correctly during test" in {
    "Hello World" shouldBe "Hello World"
  }

  it should "assert various things" in {
    1 shouldBe >=(0)
    1 shouldBe >=(1)

    List(1, 2, 3) shouldBe List(1, 2, 3)
  }
}

