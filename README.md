# Exercise

## Project description
This is an example project for recruitment purposes at [aito.ai](https://aito.ai). As such it's only an example, and does nothing noteworthy in
itself.  

The current project contains the most basic 'Hello World' sbt+scala project. It can be executed by running `./sbt run`, at which point the
program prints 'Hello World!' and any command line parameters on the command line.

`./sbt test` can be used to execute the current bdd-style tests. The test merely show the syntax, and they do nothing useful as of now.                     

The project uses [sbt-wrapper](https://github.com/paulp/sbt-extras) so there should be no need to install sbt separately, but can be executed with
the local script.

## Exercise
https://jsonplaceholder.typicode.com/photos offers a (fake) REST-API with image metadata. The API format is valid, but the data is mostly
made up. However, the data format and the links to the images do actually work, and as such this is a functioning API.

### Task
Implement, in the existing scala project structure, the following functionality:

1. Fetch the data from the API endpoint (https://jsonplaceholder.typicode.com/photos)
1. Filter the entries in the data. We're only interested in photos from the albums for which the *albumId ends with 1*, so 1, 11, 21, and so forth.
1. Change the album id to 1001 for all of these entries
1. Capitalize the initial letter in the title
1. Write the output into a file, as _minified json_. 
1. The output file should be passed in as the first and only parameter to the script
1. Make sure everything works from under sbt, when running through the command line

Submit your program with your application and cover letter, as a *tar file containing the git bare repo*. We'd like to see the version history and how you work on the problem. The
program should be executable through sbt and should run without any dependencies besides the ones specified in the `build.sbt`-build file.

Send the application and all the attachments to recruitment@aito.ai

### Restrictions and tips
* The project currently uses scala 2.12.4, and we'd like to keep it that way.
* The implementation should be done in Scala. Our core software is written in scala, and we'd like to keep it that way.
* You are free to solve the problem as you see fit. Freely add libraries or tools; create new classes, packages, files, ...
    We're interested in seeing your way of thinking and solving the problem.
* This exercise should not take very long to complete. If you have questions or suggestions, please contact us.  
* Comment, test, architect the way you see most fit.

Happy coding!
